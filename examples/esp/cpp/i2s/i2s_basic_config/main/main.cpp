#include <iostream>
#include "driver/i2s.h"

extern "C" {
void app_main();
}

int rxbuf[256], txbuf[256];
float l_in[128], r_in[128];
float l_out[128], r_out[128];

void app_main(void) {
  esp_err_t err;
  const i2s_config_t i2s_config = {
      .mode = i2s_mode_t(I2S_MODE_MASTER | I2S_MODE_RX), 
      .sample_rate = 16000, 
      .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT, 
      .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
      .communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_I2S ),
      .intr_alloc_flags = 0, 
      .dma_buf_count = 6,                         
      .dma_buf_len = 512
  };

  const i2s_pin_config_t pin_config = {
      .bck_io_num = 26,
      .ws_io_num = 25,
      .data_out_num = I2S_PIN_NO_CHANGE,
      .data_in_num = 23
  };

  i2s_config_t i2s_config_out = {
    .mode         = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX),
    .sample_rate      = 16000,
    .bits_per_sample    = I2S_BITS_PER_SAMPLE_32BIT,
    .channel_format     = I2S_CHANNEL_FMT_RIGHT_LEFT,
    .communication_format = (i2s_comm_format_t)(I2S_COMM_FORMAT_I2S ),
    .intr_alloc_flags   = 0,
    .dma_buf_count      = 6,
    .dma_buf_len      = 512
  };

  const i2s_pin_config_t i2s_speaker_pins  = {
      .bck_io_num = 13,
      .ws_io_num = 15,
      .data_out_num = 21,
      .data_in_num = I2S_PIN_NO_CHANGE
  };
  
  err = i2s_driver_install(I2S_NUM_0, &i2s_config, 0, NULL);
  if (err != ESP_OK) {
    std::cout << "Failed installing driver!" << std::endl;
    while (true);
  }
  err = i2s_set_pin(I2S_NUM_0, &pin_config);
  if (err != ESP_OK) {
    std::cout << "Failed setting pin!" << std::endl;
    while (true);
  }
  std::cout << "I2S Mic Installed!" << std::endl;

  err = i2s_driver_install(I2S_NUM_1, &i2s_config_out, 0, NULL);
  if (err != ESP_OK) {
    std::cout << "Failed installing driver!" << std::endl;
    while (true);
  }
  err = i2s_set_pin(I2S_NUM_1, &i2s_speaker_pins);
  if (err != ESP_OK) {
    std::cout << "Failed setting pin!" << std::endl;
    while (true);
  }
  
  std::cout << "I2S DAC Installed!" << std::endl;
  
	size_t readsize = 0;
  
  while (true) {
    esp_err_t rxfb = i2s_read(I2S_NUM_0,&rxbuf[0],256*4, &readsize, 1000);
    if (rxfb == ESP_OK && readsize==256*4) {
      int y=0;
      for (int i=0; i<256; i=i+2) {
        l_in[y] = (float) rxbuf[i];
        r_in[y] = (float) rxbuf[i+1];
        y++;
      }
      for (int i=0; i<128; i++) {
        l_out[i] = 0.5f * (l_in[i] + r_in[i]);
        r_out[i] = l_out[i];
      }
      y=0;
      for (int i=0;i<128;i++) {
        txbuf[y] = (int) l_out[i];
        txbuf[y+1] = (int) r_out[i];
        y=y+2;
      }
      i2s_write(I2S_NUM_1, &txbuf[0],256*4, &readsize, 1000);
    }
  }
}
