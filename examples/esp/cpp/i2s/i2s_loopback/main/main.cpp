#include <iostream>

#include "driver/gpio.h"
#include "driver/i2s.h"

#define SAMPLE_RATE 16000
const int BUFFER_LEN = 256;
int32_t RxBuffer[BUFFER_LEN], TxBuffer[BUFFER_LEN];
float LeftIn[BUFFER_LEN >> 1 ], RightIn[BUFFER_LEN >> 1 ];
float LeftOut[BUFFER_LEN >> 1 ], RightOut[BUFFER_LEN >> 1 ];

extern "C" {
void app_main();
}

auto InitMicrophone() -> bool {
  esp_err_t err;
  const i2s_config_t i2s_config = {
      .mode = i2s_mode_t(I2S_MODE_MASTER | I2S_MODE_RX), 
      .sample_rate = SAMPLE_RATE, 
      .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT, 
      .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
      .communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_I2S ),
      .intr_alloc_flags = 0, 
      .dma_buf_count = 6,                         
      .dma_buf_len = BUFFER_LEN << 1 
  };
  const i2s_pin_config_t pin_config = {
      .bck_io_num = 26,
      .ws_io_num = 25,
      .data_out_num = I2S_PIN_NO_CHANGE,
      .data_in_num = 23
  };
  err = i2s_driver_install((i2s_port_t)0, &i2s_config, 0, NULL);
  if (err != ESP_OK) {
    return false;
  }
  err = i2s_set_pin((i2s_port_t)0, &pin_config);
  if (err != ESP_OK) {
    return false;
  }
  return true;
}

auto InitDac() -> bool {
  esp_err_t err;
  const i2s_config_t i2s_config = {
      .mode = i2s_mode_t(I2S_MODE_MASTER | I2S_MODE_TX), 
      .sample_rate = SAMPLE_RATE, 
      .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT, 
      .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
      .communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_I2S),
      .intr_alloc_flags = 0, 
      .dma_buf_count = 6,                         
      .dma_buf_len = BUFFER_LEN << 1 
  };
  const i2s_pin_config_t pin_config = {
      .bck_io_num = 13,
      .ws_io_num = 15,
      .data_out_num = 21,
      .data_in_num = I2S_PIN_NO_CHANGE
  };
  err = i2s_driver_install((i2s_port_t)1, &i2s_config, 0, NULL);
  if (err != ESP_OK) {
    return false;
  }
  err = i2s_set_pin((i2s_port_t)1, &pin_config);
  if (err != ESP_OK) {
    return false;
  }
  return true;
}

auto LoopBack(size_t& read_size) -> void {
  esp_err_t rxfb = i2s_read(I2S_NUM_0,&RxBuffer[0], BUFFER_LEN << 2, &read_size, 1000);
    if (rxfb == ESP_OK && read_size == (BUFFER_LEN << 2)) {
      int32_t y=0;
      for (int32_t i = 0; i < BUFFER_LEN; i+=2) {
        LeftIn[y] = (float) RxBuffer[i];
        RightIn[y] = (float) RxBuffer[i+1];
        y++;
      }
      for (int32_t i = 0; i < BUFFER_LEN >> 1 ; i++) {
        LeftOut[i] = 0.5f * (LeftIn[i] + RightIn[i]);
        RightOut[i] = LeftOut[i];
      }
      y=0;
      for (int32_t i = 0; i < BUFFER_LEN >> 1 ; i++) {
        TxBuffer[y] = (int32_t) LeftOut[i];
        TxBuffer[y+1] = (int32_t) RightOut[i];
        y=y+2;
      }
      i2s_write(I2S_NUM_1, &TxBuffer[0], BUFFER_LEN << 2, &read_size, 1000);
    }
}

void app_main(void) {
  bool loop;
  loop = InitMicrophone();
  loop &= InitDac();

  if (loop) {
    std::cout << "I2S Interface Instaled!" << std::endl;
  } else {
    std::cout << "I2S Interface Failed!" << std::endl;
  }

  size_t read_size = 0;
  while (loop) {
    LoopBack(read_size);
  }
}
